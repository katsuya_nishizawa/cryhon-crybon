Main site is https://github.com/NZRI-AZRI/cryhon
This Gitlab version is mirror.

# crybon
crypto-bon-ozotp
CRYBON or CRYHON is ethereum-web3 based one time passward login and contens viewer app.
In this text, otp is one time passward .

2020-07-23 (2020年7月23日改訂)

GitLab ver is https://gitlab.com/katsuya_nishizawa/cryhon-crybon.


BON or HON mean book(本) in japanese.

The copyright of the code belongs to Katsuya Nishizawa.
(コードの著作権はKatsuya Nishizawaに帰属します。)

The copyright of the external Javascript module used is owned by each developer.
(また使用している外部Javascriptモジュールの著作権はそれぞれの開発者に帰属しています。)

※外部モジュールがLGPL3やMITライセンスの為、暗黙のうちにそのライセンスをこのアプリやコードは継承します。
初回公開時はweb3.jsがLGPL3ライセンスを含んでいたのでLGPL3ライセンスを無視できません。
（LGPL3を含むためいわゆる権利を放棄したUNLICENSEにはなり得ないことをここに記します。web3の製作者は権利を放棄したわけではありません。重ねて記述しておきますがこのプロジェクトでのUNLICENSEDは　誰にもコードの権利をライセンスしないことを示します。）

7月13日初回公開の時点ではだれにも利用を許可しないことを前提に公開しています。
7月23日改訂時に、UNLICENSEの表記の意味が思っていたものと違ったので、ライセンスについてプロプライエタリソフトウェアであり、非フリーソフトウェアで、かつオープンソースであることを追記しました。

このプロジェクトは開発中であり、このコードには未完成の項目や意図しないセキュリティホールが存在しえます。
現在のソースコードを無断で引用し電子書籍を作ろうとすることは、あなたやあなたの所属する会社の品位を落としかねません。

コードの権利者である私は一切の補償をしません。


Make flow is ... [node.js  --> Electron --> electron-builder-->Make 1 app file including Javascript-Html-Css-pdf-mp3-mp4-etc.. ---> 1 File such type are " exe-dmg-app ".]

/*---------*/
Javascript and HTML file include this..
@web3.js
@otp token  contract address [0x--------]
@private key(this ethereum key have otp token . token based erc-721. ) [bytes32 type like... 0x--------]
@web3 websocketURI [URI type like... wss:--------]
@set.js/set.html  [first. set private key and web3 websocketURI. ]
@otp.js/otp.html  [second. accses ethereum network via websocketURI.]
@bon.js/bon.html  [(hon.js/hon.html) third. If one-time password authentication result is true , you are this crybon owner . you can read contents.
 content file are "html-txt , pdf , mp3 sound , mp4 movie , image file(comic image , manga-bon image) "]


/*---------*/


2020-8-1追記
＜コードがフリーソフトではない理由＞
フリーソフトにしない理由はこのソフトウェアが悪用されないようにするためです。秘密裏に暗号化された万人に好ましくない内容の本のファイルがばらまかれ、無法地帯になることが無いよう、利用に制限をかけます。（このアプリの形式で製本する際にゲームにおける日本のCEROのようなレーティング団体が必要だと考えています。）

筆者はイーサリアム、Solidity言語、そしてweb3.jsなどのGPLライセンスやMITライセンスの精神に支えられた外部モジュールを駆使してこのソフトウェアを開発すること出来ていますが、出来上がったソフトウェアの用途と可能性を踏まえ、利用を制限させていただきます。このソフトウェアの商用及び個人での利用を禁じます。

このコードは誰にもライセンスしません。私のみライセンスされ、ほかの人はアンライセンスされます。
このコードを利用したい人は私の許可を受けてください。

